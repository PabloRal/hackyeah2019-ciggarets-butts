import 'package:flutter/material.dart';
import 'package:pety/UI/Appbar.dart';
import 'package:pety/Models/Event.dart';
import 'package:pety/Repository.dart';
import 'package:pety/RelationForm.dart';

class EventPage extends StatefulWidget {
  EventPage(this.event, this.repo);

  final Event event;
  final Repository repo;

  @override
  _EventPageState createState() => _EventPageState();
}

class _EventPageState extends State<EventPage> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: RoundedAppBar(title: widget.event.title,),
      body: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    'Co robimy?',
                    style: TextStyle(
                        fontSize: 25
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    widget.event.description,
                    style: TextStyle(
                        fontSize: 20
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    'Gdzie?',
                    style: TextStyle(
                        fontSize: 25
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    widget.event.location,
                    style: TextStyle(
                        fontSize: 20
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: RaisedButton(
                    padding: EdgeInsets.all(10),
                    color: Colors.green,
                    onPressed: () {
                      setState((){widget.repo.saveEvent(widget.event);});

                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Center(
                      child: Text(
                        widget.event.saved ? 'Wypisz się z eventu' : 'Zapisz event',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                        ),
                      ),

                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: RaisedButton(
                    color: Colors.green,
                    padding: EdgeInsets.all(10),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: RouteSettings(name: '/relation-form'),
                          builder: (context) => RelationForm(widget.repo, widget.event.id),
                        ),
                      );

                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Center(
                      child: Text(
                        'Dodaj relacje',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.0,
                        ),
                      ),

                    ),
                  ),
                )
              ],
            ),
          )

        ],
      )
    );
  }
}