import 'package:flutter/material.dart';
import 'package:pety/UI/Appbar.dart';
import 'package:pety/UI/VideosList.dart';
import 'package:pety/UI/EventsList.dart';
import 'package:pety/UI/SavedEvents.dart';
import 'package:pety/Repository.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  List<Widget> _pages = [];
  Repository repo = Repository();


  @override
  void initState() {
    super.initState();
    _pages = [
      VideosList(repo),
      EventsList(repo),
      SavedEvents(repo)
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RoundedAppBar(title: widget.title),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: _pages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.video_label),
            title: Text('Relacje'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.event),
            title: Text('Eventy'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            title: Text('Zapisane'),
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
