import 'package:flutter/material.dart';
import 'package:pety/EventPage.dart';
import 'package:pety/Models/Event.dart';
import 'package:pety/EventForm.dart';
import 'package:pety/Repository.dart';

class EventsList extends StatefulWidget {
  EventsList(this.repo);

  final Repository repo;

  @override
  _EventsListState createState() => _EventsListState();
}

class _EventsListState extends State<EventsList> {
  List<Event> events;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<List<Event>>(
      future: widget.repo.fetchEvents(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          events = snapshot.data;

          return ListView.builder(
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              if (index == 0) {
                return Column(children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: RaisedButton(
                      color: Colors.green,
                      padding: EdgeInsets.all(10),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            settings: RouteSettings(name: '/event-form'),
                            builder: (context) => EventForm(widget.repo),
                          ),
                        );
                      },
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      ),
                      child: Center(
                        child: Text(
                          'Dodaj event',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: RouteSettings(name: '/event-page'),
                          builder: (context) =>
                              EventPage(snapshot.data[index], widget.repo),
                        ),
                      );
                    },
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: Text(snapshot.data[index].title),
                          subtitle: Text(snapshot.data[index].subtitle),
                        ),
                        Container(
                          color: Colors.grey[300],
                          height: 1.0,
                        )
                      ],
                    ),
                  )
                ]);
              }
              return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        settings: RouteSettings(name: '/event-page'),
                        builder: (context) =>
                            EventPage(snapshot.data[index], widget.repo),
                      ),
                    );
                  },
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        title: Text(snapshot.data[index].title),
                        subtitle: Text(snapshot.data[index].subtitle),
                      ),
                      Container(
                        color: Colors.grey[300],
                        height: 1.0,
                      )
                    ],
                  ));
            },
          );
        }
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: RaisedButton(
            color: Colors.green,
            padding: EdgeInsets.all(0),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  settings: RouteSettings(name: '/event-form'),
                  builder: (context) => EventForm(widget.repo),
                ),
              );
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: Center(
              child: Text(
                'Dodaj event',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
