import 'package:flutter/material.dart';
import 'package:pety/EventPage.dart';
import 'package:pety/Models/Event.dart';
import 'package:pety/Repository.dart';


class SavedEvents extends StatefulWidget {
  SavedEvents(this.repo);

  final Repository repo;

  @override
  _SavedEventsState createState() => _SavedEventsState();
}

class _SavedEventsState extends State<SavedEvents> {

  List<Event> events;


  @override
  void initState() {
    super.initState();
    events = widget.repo.getSavedEvents();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
      itemCount: events.length,
      itemBuilder: (context, index) {
        return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  settings: RouteSettings(name: '/event-page'),
                  builder: (context) => EventPage(events[index], widget.repo),
                ),
              );
            },
            child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(events[index].title),
                  subtitle: Text(events[index].subtitle),
                ),
                Container(
                  color: Colors.grey[300],
                  height: 1.0,
                )
              ],
            )
        );
      },
    );
  }
}