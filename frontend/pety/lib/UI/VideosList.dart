import 'package:flutter/material.dart';
import 'package:pety/RelationPage.dart';
import 'package:pety/Models/Relation.dart';
import 'package:pety/Repository.dart';

class VideosList extends StatefulWidget {
  VideosList(this.repo);

  final Repository repo;

  @override
  _VideosListState createState() => _VideosListState();
}

class _VideosListState extends State<VideosList> {


  List<Relation> relations;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    return FutureBuilder<List<Relation>> (
        future: widget.repo.fetchRelations(),
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        return GridView.builder(
            itemCount: snapshot.data.length,
            gridDelegate:
            new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 2 / 3),
            itemBuilder: (context, index) {
              return Padding(
                  padding: EdgeInsets.all(5),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          settings: RouteSettings(name: '/relation-page'),
                          builder: (context) => RelationPage(snapshot.data[index]),
                        ),
                      );
                    },
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Text(
                              snapshot.data[index].title,
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                  shadows: [
                                    Shadow( // bottomLeft
                                        offset: Offset(-0.35, -0.35),
                                        color: Colors.black
                                    ),
                                    Shadow( // bottomRight
                                        offset: Offset(0.35, -0.35),
                                        color: Colors.black
                                    ),
                                    Shadow( // topRight
                                        offset: Offset(0.35, 0.35),
                                        color: Colors.black
                                    ),
                                    Shadow( // topLeft
                                        offset: Offset(-0.35, 0.35),
                                        color: Colors.black
                                    ),
                                  ]
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Text(
                              'Punkty: ${snapshot.data[index].points.toString()}',
                              style: TextStyle(
                                  fontSize: 17,
                                  color: Colors.white,
                                  shadows: [
                                    Shadow( // bottomLeft
                                        offset: Offset(-0.35, -0.35),
                                        color: Colors.black
                                    ),
                                    Shadow( // bottomRight
                                        offset: Offset(0.35, -0.35),
                                        color: Colors.black
                                    ),
                                    Shadow( // topRight
                                        offset: Offset(0.35, 0.35),
                                        color: Colors.black
                                    ),
                                    Shadow( // topLeft
                                        offset: Offset(-0.35, 0.35),
                                        color: Colors.black
                                    ),
                                  ]
                              ),
                            ),
                          )
                        ],
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        image: DecorationImage(
                          fit: BoxFit.fitHeight,
                          image: NetworkImage(snapshot.data[index].imageUrl),
                        ),
                      ),
                    ),
                  )
              );
            });
      } else {
        return Center(child: CircularProgressIndicator());
      }
    }
    );





  }
}
