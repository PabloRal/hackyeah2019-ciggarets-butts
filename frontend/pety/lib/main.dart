import 'package:flutter/material.dart';
import 'package:pety/HomePage.dart';
import 'package:flutter/services.dart';


void main() => {
SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
statusBarColor: Colors.white, //or set color with: Color(0xFF0000FF)
)),
  runApp(MyApp())
};

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primaryColor: Colors.lime[500],
        primarySwatch: Colors.green
      ),
      home: HomePage(title: 'Wysprzątajmy świat'),
    );
  }
}

