package events

import (
	"errors"
)

type EventSign struct {
	EventID uint32 `json:"eventId"`
}

type Signer interface {
	Sign(userID uint32, eventID uint32) error
}

type Displayer interface {
	Display(uint32) ([]EventSign, error)
}

type Account interface {
	Signer
	Displayer
}

type MemoryAccount struct {
	UserSignedEvents map[uint32][]EventSign
	Retriever        Retriever
}

func (m *MemoryAccount) Sign(userID uint32, eventID uint32) error {
	_, exists := m.UserSignedEvents[userID]
	if !exists {
		m.UserSignedEvents[userID] = []EventSign{}
	}

	_, err := m.Retriever.Retrieve(uint32(eventID))
	if err != nil {
		return err
	}

	m.UserSignedEvents[userID] = append(m.UserSignedEvents[userID], EventSign{EventID: eventID})

	return nil
}

func (m *MemoryAccount) Display(userID uint32) ([]EventSign, error) {
	if m == nil {
		return nil, errors.New("storage unreachable, cannot save transaction")
	}

	events, exists := m.UserSignedEvents[userID]
	if !exists {
		return []EventSign{}, nil
	}
	return events, nil
}

func NewAccount() (Account, error) {
	return &MemoryAccount{}, nil
}
