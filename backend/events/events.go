package events

import "errors"

type Event struct {
	Id           uint32 `json:"id"`
	MaintainerId uint32 `json:"maintainerId"`
	Title        string `json:"title"`
	Subtitle     string `json:"subtitle"`
	Description  string `json:"description"`
	Localization string `json:"localization"`
}

type Lister interface {
	List() ([]Event, error)
}

type Inserter interface {
	Insert(Event) error
}

type Retriever interface {
	Retrieve(uint32) (Event, error)
}

type Storage interface {
	Lister
	Inserter
	Retriever
}

type MemoryStorage struct {
	Events []Event
}

func (m *MemoryStorage) List() ([]Event, error) {
	if m == nil {
		return nil, errors.New("storage unreachable, cannot list events")
	}

	return m.Events, nil
}

func (m *MemoryStorage) Insert(event Event) error {
	if m == nil {
		return errors.New("storage unreachable, cannot insert new one")
	}

	id := uint32(len(m.Events))
	event.Id = id
	m.Events = append(m.Events, event)

	return nil
}

func (m *MemoryStorage) Retrieve(id uint32) (Event, error) {
	if m == nil {
		return Event{}, errors.New("storage unreachable, cannot insert new one")
	}

	if id >= uint32(len(m.Events)) {
		return Event{}, errors.New("event with given id doesn't exists")
	}

	return m.Events[id], nil
}

func NewStorage() (Storage, error) {
	return &MemoryStorage{}, nil
}
