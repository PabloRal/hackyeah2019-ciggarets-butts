package stories

import "errors"

type Story struct {
	Id           uint32 `json:"id"`
	MaintainerId uint32 `json:"maintainerId"`
	Title        string `json:"title"`
	Subtitle     string `json:"subtitle"`
	Description  string `json:"description"`
	VideoUrl     string `json:"videoUrl"`
	ImageUrl     string `json:"imageUrl"`
	Localization string `json:"localization"`
}

type Lister interface {
	List() ([]Story, error)
}

type Inserter interface {
	Insert(Story) error
}

type Retriever interface {
	Retrieve(uint32) (Story, error)
}

type Storage interface {
	Lister
	Inserter
	Retriever
}

type MemoryStorage struct {
	Stories []Story
}

func (m *MemoryStorage) List() ([]Story, error) {
	if m == nil {
		return nil, errors.New("storage unreachable, cannot list Stories")
	}

	return m.Stories, nil
}

func (m *MemoryStorage) Insert(story Story) error {
	if m == nil {
		return errors.New("storage unreachable, cannot insert new one")
	}

	id := uint32(len(m.Stories))
	story.Id = id
	m.Stories = append(m.Stories, story)

	return nil
}

func (m *MemoryStorage) Retrieve(id uint32) (Story, error) {
	if m == nil {
		return Story{}, errors.New("storage unreachable, cannot insert new one")
	}

	if id >= uint32(len(m.Stories)) {
		return Story{}, errors.New("Story with given id doesn't exists")
	}

	return m.Stories[id], nil
}

func NewStorage() (Storage, error) {
	return &MemoryStorage{}, nil
}
