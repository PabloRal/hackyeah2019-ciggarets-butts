package users

import "errors"

type User struct {
	UserID uint32 `json: userId"`
}

type Registrar interface {
	Register(uint32) error
}

type MemoryRegistrar struct {
	Users []uint32
}

func (m *MemoryRegistrar) Register(userID uint32) error {
	if m == nil {
		return errors.New("storage unreachable")
	}

	if contains(m.Users, userID) {
		return errors.New("For this userID some user is already registered")
	}

	return nil
}

func contains(s []uint32, x uint32) bool {
	for _, a := range s {
		if a == x {
			return true
		}
	}
	return false
}

func NewStorage() (Registrar, error) {
	return &MemoryRegistrar{}, nil
}
