package server

import (
	"encoding/json"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/make-poland-cleaner/events"
	"gitlab.com/make-poland-cleaner/stories"
)

func (s *server) routes(r *mux.Router) {
	r.HandleFunc("/status", s.status())
	r.HandleFunc("/events", s.getEvents()).Methods("GET")
	r.HandleFunc("/events", s.postEvent()).Methods("POST")
	r.HandleFunc("/events/{id}", s.getEvent()).Methods("GET")
	r.HandleFunc("/events/{id}/sign", s.signForEvent()).Methods("POST")
	r.HandleFunc("/events/{id}/sendProves", s.sendEventProves()).Methods("POST")
	r.HandleFunc("/stories", s.getStories()).Methods("GET")
}

type statusResponse struct {
	Uptime time.Duration `json:"uptime"`
}

func (s *server) status() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusOK
		resp := statusResponse{
			Uptime: time.Since(s.timer),
		}

		respJSON, err := json.Marshal(resp)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(respJSON)
	}
}

type getEventsResponse struct {
	Events []events.Event `json:"events"`
}

func (s *server) getEvents() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusOK

		events, err := s.eventsStorage.List()
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		resp := getEventsResponse{events}
		respJSON, err := json.Marshal(resp)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(respJSON)
	}
}

func (s *server) postEvent() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusCreated
		decoder := json.NewDecoder(r.Body)
		var eventData events.Event

		err := decoder.Decode(&eventData)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = s.eventsStorage.Insert(eventData)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
	}
}

func (s *server) getEvent() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusOK
		vars := mux.Vars(r)

		eventID, ok := vars["id"]
		if !ok {
			http.Error(w, "Wrong fromat of event's id", http.StatusBadRequest)
			return
		}

		id, err := strconv.Atoi(eventID)
		if err != nil {
			http.Error(w, "Event's id must be a number", http.StatusBadRequest)
			return
		}

		eventData, err := s.eventsStorage.Retrieve(uint32(id))
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		respJSON, err := json.Marshal(eventData)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(respJSON)
	}
}

type signUpForEventRequest struct {
	UserID uint32 `json:"userId`
}

func (s *server) signForEvent() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusCreated
		vars := mux.Vars(r)

		eventID, ok := vars["id"]
		if !ok {
			http.Error(w, "Wrong fromat of event's id", http.StatusBadRequest)
			return
		}

		id, err := strconv.Atoi(eventID)
		if err != nil {
			http.Error(w, "Event's id must be a number", http.StatusBadRequest)
			return
		}

		decoder := json.NewDecoder(r.Body)
		var signData signUpForEventRequest

		err = decoder.Decode(&signData)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = s.accountData.Sign(signData.UserID, uint32(id))
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
	}
}

type eventProvesRequest struct {
	VideoUrl string `jsos:"videoUrl"`
	ImageUrl string `jsos:"imageUrl`
}

func (s *server) sendEventProves() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusCreated
		vars := mux.Vars(r)

		eventID, ok := vars["id"]
		if !ok {
			http.Error(w, "Wrong fromat of event's id", http.StatusBadRequest)
			return
		}

		id, err := strconv.Atoi(eventID)
		if err != nil {
			http.Error(w, "Event's id must be a number", http.StatusBadRequest)
			return
		}

		decoder := json.NewDecoder(r.Body)
		var proveData eventProvesRequest

		err = decoder.Decode(&proveData)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		eventData, err := s.eventsStorage.Retrieve(uint32(id))
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		storyData := stories.Story{
			MaintainerId: eventData.MaintainerId,
			Title:        eventData.Title,
			Subtitle:     eventData.Subtitle,
			Description:  eventData.Description,
			VideoUrl:     proveData.VideoUrl,
			ImageUrl:     proveData.ImageUrl,
			Localization: eventData.Localization,
		}

		err = s.storiesStorage.Insert(storyData)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
	}
}

type getStoriesResponse struct {
	Stories []stories.Story `json:"stories"`
}

func (s *server) getStories() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		statusCode := http.StatusOK

		stories, err := s.storiesStorage.List()
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		resp := getStoriesResponse{stories}
		respJSON, err := json.Marshal(resp)
		if err != nil {
			s.logger.Println(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		w.Write(respJSON)
	}
}
