package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/make-poland-cleaner/events"
	"gitlab.com/make-poland-cleaner/stories"
	"gitlab.com/make-poland-cleaner/users"
)

type server struct {
	logger         *log.Logger
	timer          time.Time
	eventsStorage  events.Storage
	storiesStorage stories.Storage
	usersStorage   users.Registrar
	accountData    events.Account
}

type key int

const (
	requestIDKey key = 0
)

func tracing(nextRequestID func() string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			requestID := r.Header.Get("X-Request-Id")
			if requestID == "" {
				requestID = nextRequestID()
			}
			ctx := context.WithValue(r.Context(), requestIDKey, requestID)
			w.Header().Set("X-Request-Id", requestID)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func New() http.Handler {
	logger := log.New(os.Stdout, "http: ", log.LstdFlags)

	eventsStorage, err := events.NewStorage()
	if err != nil {
		logger.Fatal()
	}

	usersStorage, err := users.NewStorage()
	if err != nil {
		logger.Fatal()
	}

	accountData, err := events.NewAccount()
	if err != nil {
		logger.Fatal()
	}

	storiesStorage, err := stories.NewStorage()
	if err != nil {
		logger.Fatal()
	}

	s := server{
		logger:         logger,
		timer:          time.Now(),
		eventsStorage:  eventsStorage,
		storiesStorage: storiesStorage,
		usersStorage:   usersStorage,
		accountData:    accountData,
	}

	s.usersStorage.Register(0)

	router := mux.NewRouter()
	s.routes(router)

	nextRequestID := func() string {
		return fmt.Sprintf("%d", time.Now().UnixNano())
	}

	c := []string{
		"http://localhost:4200",
	}

	handler := tracing(nextRequestID)(handlers.CORS(
		handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}),
		handlers.AllowCredentials(),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}),
		handlers.AllowedOrigins(c))(logging(logger)(router)))

	return handler
}
