package main

import (
	"log"
	"net/http"

	"gitlab.com/make-poland-cleaner/server"
)

func main() {
	handler := server.New()

	httpServer := &http.Server{
		Addr:    ":8080",
		Handler: handler,
	}

	log.Fatal(httpServer.ListenAndServe())
}
